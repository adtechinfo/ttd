# TTD 
TTD showed this to some SSPs at Cannes.  It's important for the rest of the industry to know how TTD and a few others are trying to squash competition and force their cookie on competitors. 

TTD is forcing our supply-side partners to stop syncing with others. They say DSPs will have to use their cookie, but they can take it away whenever they want.

TTD wants its competitors to move to its cookie and remove control of the single most important part of a bid decision – their cookie.